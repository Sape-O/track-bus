import React, { Component } from 'react';
import {Route,Switch,Link} from 'react-router-dom';
import {Bus} from './bus/bus';
import {Chauffeur} from './chauffeur/chauffeur';
import {Document} from './document/document';
import {Home} from './home/home';
import {Promotion} from './promotion/promotion';
import {RouteBus} from './route/route';
import {Run} from './run/run';
import {Signin} from './signin/signin';
import {Sound} from './sound/sound';
import {Station} from './station/station';
import {Student} from './student/student';



import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

import './App.css';

export default class App extends Component {

  render(){
    if(true){
      return(
        <div>
          <nav class="navbar is-primary" role="navigation" aria-label="main navigation" >
            <div id="navbarBasicExample" class="navbar-menu">
              <div class="navbar-start">
                <Link to="/" class="navbar-item">SUT Track BUS</Link>

              </div>
              <div class="navbar-end">

                <Link to="/home" class="navbar-item">Home</Link>
                <Link to="/document" class="navbar-item">Documentation</Link>
                <Link to="/bus" class="navbar-item">BUS</Link>
                <Link to="/chauffeur" class="navbar-item">Chauffeur</Link>
                <Link to="/station" class="navbar-item">Station</Link>
                <Link to="/sound" class="navbar-item">Sound</Link>
                <Link to="/route" class="navbar-item">Route</Link>
                <Link to="/run" class="navbar-item">run</Link>
                <Link to="/student" class="navbar-item">Student</Link>
                <Link to="/promotion" class="navbar-item">Promotion</Link>

                <div class="navbar-item">
                  <div class="buttons">
                    <Link to="/signin" class="button is-success">Sign Up</Link>
                    <Link to="/signup" class="button is-success">Sign In</Link>
                  </div>
                </div>
              </div>
            </div>
          </nav>
          this is "/"
          <React.Fragment>
            <Route path="/home" component={Home} />
            <Route path="/document" component={Document} />
            <Route path="/bus" component={Bus}/>
            <Route path="/chauffeur" component={Chauffeur}/>
            <Route path="/station" component={Station}/>
            <Route path="/sound" component={Sound}/>
            <Route path="/route" component={RouteBus}/>
            <Route path="/run" component={Run}/>
            <Route path="/student" component={Student}/>
            <Route path="/promotion" component={Promotion}/>

            <Route path="/signin" component={Signin} />
          </React.Fragment>
        </div>
      );
    }
  }
}
