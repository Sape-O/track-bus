import React,{Component} from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Button} from 'primereact/button';
//import {CarService} from '../service/CarService'; // ??

// Source framework primereact=> https://www.primefaces.org/primereact/#/datatable/templating  ==>template main
// https://www.primefaces.org/primereact/#/datascroller/inline  ==> load data when scolldown
// https://www.primefaces.org/primereact/#/datatable/filter ==> filter search
export class Chauffeur extends Component {
  constructor() {
    super();
    this.state = {
      cars: []
    };
  //  this.carservice = new CarService();
    this.brandTemplate = this.brandTemplate.bind(this);
    this.colorTemplate = this.colorTemplate.bind(this);
    this.actionTemplate = this.actionTemplate.bind(this);
  }

  colorTemplate(rowData, column) {// show swap color of rowData
    return <span style={{color: rowData['color']}}>{rowData['color']}</span>;
  }

  brandTemplate(rowData, column) {
     // show same LightBoox => https://www.primefaces.org/primereact/#/lightbox
     var src = "showcase/resources/demo/images/car/" + rowData.brand + ".png";
     return <img src={src} alt={rowData.brand} width="48px" />;
  }

  actionTemplate(rowData, column) {
    return <div>
            <Button type="button" icon="pi pi-search" className="p-button-success" style={{marginRight: '.5em'}}></Button>
            <Button type="button" icon="pi pi-pencil" className="p-button-warning"></Button>
            <Button type="button" icon="pi pi-delete" className="p-button-dangerous"></Button>//delete
          </div>;
  }

  componentDidMount() {
    //getdata from https:// any web site
  //  this.carservice.getCarsSmall().then(data => this.setState({cars: data}));
  }

  render() {
    var carCount = this.state.cars ? this.state.cars.length: 0;
    var header = <div className="p-clearfix" style={{'lineHeight':'1.87em'}}>Chauffeur<Button icon="pi pi-refresh" style={{'float':'right'}}/></div>;
    var footer = "There are " + carCount + ' cars';

    return (
             <div>
                <div className="content-section introduction">
                  <div className="feature-intro">
                    <h1>คนขับรถ</h1>
                    <p>อธิบายเกี่ยวกับคนขับรถ</p>
                  </div>
                </div>

                <div className="content-section implementation">
                  <DataTable value={this.state.cars} header={header} footer={footer}>
                    <Column field="No" header="No." />
                    <Column field="Name" header="Name" />
                    <Column field="Surname" header="Surname" body={this.brandTemplate} style={{textAlign:'center'}}/>
                    <Column field="color" header="Color" body={this.colorTemplate} />
                    <Column body={this.actionTemplate} style={{textAlign:'center', width: '8em'}}/>
                  </DataTable>
                </div>
             </div>
         );
     }
}
