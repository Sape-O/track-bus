import React, {Component} from 'react';
import {DataView, DataViewLayoutOptions} from 'primereact/dataview';
import {FileUpload} from 'primereact/fileupload';
import {Growl} from 'primereact/growl';
import {OverlayPanel} from 'primereact/overlaypanel';

import {InputText} from 'primereact/inputtext';
import {Password} from 'primereact/password';
import {Button} from 'primereact/button';

import 'primereact/resources/themes/nova-dark/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

import 'bulma/css/bulma.css';
import 'bulma/css/bulma.min.css';
import './bus.css';

export class Bus extends Component{
  constructor() {
      super();
      this.state={
        numcar:null,
        license_plate:null,
        seat:null,
        username:null,
        password:null,

        visible: false
      }

    }



  render(){

    return(
      <div>
        <br/>
          <div className="content-section implementation">
            <Button type="button" label="Toggle" onClick={(e) => this.op.toggle(e)} />
              <OverlayPanel ref={(el) => this.op = el}><br/>

                <span className="p-float-label">
                  <InputText
                    id="numcar"
                    type="text"
                    style={{width:"90%"}}
                    value={this.state.numcar}
                    onChange={(e) => this.setState({numcar: e.target.value})}
                  />
                <label htmlFor="seat">seat</label>
              </span><br/>
              <span className="p-float-label">
                  <InputText
                    id="license_plate"
                    type="text"
                    style={{width:"90%"}}
                    value={this.state.license_plate}
                    onChange={(e) => this.setState({license_plate: e.target.value})}
                  />
                <label htmlFor="seat">seat</label>
              </span><br/>
                <span className="p-float-label">
                  <InputText
                    id="seat"
                    style={{width:"90%"}}
                    value={this.state.seat}
                    onChange={(e) => this.setState({seat: e.target.value})}
                  />
                  <label htmlFor="seat">seat</label>
                </span><br/>
                <span className="p-float-label">
                  <InputText
                    id="username"
                    style={{width:"90%"}}
                    value={this.state.username}
                    onChange={(e) => this.setState({username: e.target.value})}
                    />
                  <label htmlFor="username">Username</label>
                </span><br/>
                <Password
                  style={{width:"90%"}}
                  value={this.state.password}
                  onChange={(e) => this.setState({password: e.target.value})}
                  placeholder="Password"/><br/>
                <FileUpload
                  mode="basic"
                  name="demo[]"
                  uurl="./upload.php"
                  accept="image/*"
                  maxFileSize={1000000}
                  onUpload={this.onBasicUploadAuto}
                  auto={true}
                  chooseLabel="Picture" />&nbsp;&nbsp;

              </OverlayPanel>
          </div>
        </div>

    );
  }



}
